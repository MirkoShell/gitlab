import mountDashboards from 'ee/analytics/analytics_dashboards/mount_dashboards';
import DashboardsApp from 'ee/analytics/analytics_dashboards/dashboards_app.vue';

mountDashboards('js-analytics-dashboards-list-app', DashboardsApp);
